import React, { Component } from 'react';
import addForm from './components/AddForm/AddForm';
import messages from './components/messages/Messages';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <addForm/>
        <messages/>
      </div>
    );
  }
}

export default App;
