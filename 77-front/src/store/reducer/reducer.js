import {
    MESSAGES_SUCCESS,

} from "../action/actions";

const initialState ={
    messages: [],
};



const messageReducer =(state = initialState, action) =>{
    switch (action.type) {
        case MESSAGES_SUCCESS:
            return{
                ...state,
                messages: action.messages
            };
        default:
            return state;
    }
};

export default messageReducer;